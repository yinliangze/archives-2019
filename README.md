# Repository Description

This repository hosts the verifier archives for
the International Competition on Software Verification (SV-COMP).

# Reduction of Repository Size:

Note (dbeyer 2018-11-19):
To reduce the size of the repository (we were at 11.8 GB, while the (soft) quota is 10 GB),
I removed the largest three archives from the repository, using the following commands,
and ask the three teams to resubmit the latest version:

`git filter-branch --index-filter 'git rm --cached --ignore-unmatch 2019/skink.zip 2019/veriabs.zip 2019/map2check.zip' HEAD`

`git push --force`

